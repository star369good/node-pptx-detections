var PPTX = require('./lib/pptx');
var fs = require('fs');

var INFILE = './test/files/test.pptx';

fs.readFile(INFILE, function (err, data) {
    if (err) throw err;

    var pptx = new PPTX.Presentation();
    pptx.load(data, function (err) {

        var slide1 = pptx.getSlide('slide1');
        var shapes = slide1.getShapes();

        // console.log(shapes[0].text(), shapes[0].shapeProperties().x(), shapes[0].shapeProperties().y(), 
        //     shapes[0].shapeProperties().cx(), shapes[0].shapeProperties().cy());
        console.log(shapes[1].text(), shapes[1].shapeProperties().x(), shapes[1].shapeProperties().y(), 
            shapes[1].shapeProperties().cx(), shapes[1].shapeProperties().cy());
        // console.log(shapes[2].text(), shapes[2].shapeProperties().x(), shapes[2].shapeProperties().y(), 
        //     shapes[2].shapeProperties().cx(), shapes[2].shapeProperties().cy());

        // shapes[3]
        //     .text("Now it's a trapezoid")
        //     .shapeProperties()
        //     .x(PPTX.emu.inch(1))
        //     .y(PPTX.emu.inch(1))
        //     .cx(PPTX.emu.inch(2))
        //     .cy(PPTX.emu.inch(0.75))
        //     .prstGeom('trapezoid');
    });
});